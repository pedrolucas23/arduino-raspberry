from django.shortcuts import render
from django.http import HttpResponse
from django.http import HttpResponseRedirect
import sys
import RPi.GPIO as GPIO
import time
import serial
from serial import Serial

# Create your views here.

def index(request):
    return render(request, 'webapp/home.html')

GPIO.setmode(GPIO.BCM)
GPIO.setwarnings(False)
#StepPins = [21, 20, 16, 12]
#LedPins = [17, 18, 2, 3]

ser = serial.Serial('/dev/ttyUSB0', 9600)
#A segunda janela nao foi implementada por nao haver outro motor de passo

#for pin in StepPins:
#    GPIO.setup(pin, GPIO.OUT)
#    GPIO.output(pin, False)

#for pin in LedPins:
#    GPIO.setup(pin, GPIO.OUT)
#    GPIO.output(pin, False)

#Seq = [[1,0,0,0],
#       [1,1,0,0],
#       [0,1,0,0],
#       [0,1,1,0],
#       [0,0,1,0],
#       [0,0,1,1],
#       [0,0,0,1],
#       [1,0,0,1]]

#StepCount = len(Seq) - 1

#WaitTime = 1/float(400)

def temperatura(request):
#    if 'on' in request.POST:
#        GPIO.output(18, GPIO.HIGH)
#    elif 'off' in request.POST:
#        GPIO.output(18, GPIO.LOW)
#    elif "on2" in request.POST:
#        GPIO.output(17, GPIO.HIGH)
#    elif "off2" in request.POST:
#        GPIO.output(17, GPIO.LOW)

    request.GET = ser.readline()
            
    return render(request, 'webapp/quarto1.html')


