from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^temperatura/$', views.temperatura, name='temperatura'),
#    url(r'^quarto_2/$', views.quarto_2, name='quarto_2'),
]
